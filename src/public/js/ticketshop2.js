/**
 * Url of the fluxcess RSVP application (API / web service)
 */
var fluxcessRsvpUrl = '###fluxcess_rsvp_domain###/api_1.0/de/';

/**
 * Url of the fluxcess Tickets application
 */
var fluxcessTicketsUrl = '###fluxcess_tickets_domain###';


/**
 * Do not change the code below.
 */


/**
 * fluxcess rsvp session id
 */
var fluxcessSessionId = '';

/**
 * shopping cart id is maintained by the server.
 */
var bookingId = { };

/**
 * ticket categories
 */
var ticketcategories = { };

/**
 * Is true once the guest data is available
 * will be indexed with the uuid of the form. Example:
 * 
 * flGuestDataLoaded['uid29304809384092'] = true
 */
var flGuestDataLoaded = [];

/**
 * Guest Data, if loaded via ajax/json
 * will be indexed with the uid of the form. Example:
 * 
 * flGuestData['uid29304809384092'] = []
 */
var flGuestData = [];

/**
 * Load fluxcess session id from cookie
 */
fluxcessSessionId = getCookie('fluxcessSessionId')

/**
 * select event
 */
$(document).on('click',
	'.ticketshop div.eventposter .reglinked',
	function() {
		var eventid = $(this).parent().data('id');
		createUniqueId($(this).parent().parent());
		var destination = "ticketshop/" + $(this).parent().parent().attr("id") + "/" + eventid;
		document.location.hash = destination;
	});

/**
 * select payment type
 */
$(document).on('click', '.paymenttype ul li', function() {
		var paymenttype_id = $(this).data('paymenttype_id');
		selectPaymenttype($(this), paymenttype_id);
	});

/**
 * select shipping type
 */
$(document).on('click', '.shippingtype ul li', function() {
		var shippingtype_id = $(this).data('shippingtype_id');
		selectShippingtype($(this), shippingtype_id);
	});

/**
 * show cart (step 1)
 */
$(document).on('click', '.toCart', function() {
		var section = $(this).parentsUntil('[data-eventid]').parent();
		var eventid = $(section).data('eventid');
		menuPointer(section, 1);
		$(section).find('.shoppingcart').show();
		$(section).find('.choice').show();
		$(section).find('.addresses').hide();
		$(section).find('.summary').hide();
		$(section).find('.toCheckout.btn').show();
	});

/**
 * show checkout (step 2)
 */
$(document).on('click',
	'.toCheckout',
	function() {
		var eventid = $(this).parentsUntil('[data-eventid]').parent().data('eventid');
		var section = $(this).parentsUntil('[data-eventid]').parent();
		if ($(section).find('.cart').data('filled') == 1) {
			goCheckout(this, eventid);
			menuPointer(section, 2);
			$(section).find('.toCheckout.btn').hide();
			$(section).find('.addresses').show();
			$(section).find('.shoppingcart').show();
			$(section).find('.summary').hide();
		} else {
			alert("Der Warenkorb ist noch leer.");
		}
	});

/**
 * show summary (step 3)
 */
$(document).on('submit',
	'.addressform',
	function() {
		if (bookingId != null) {
			var section = $(this).parentsUntil('[data-eventid]').parent();
			$(section).find('.errors').remove();
			$(section).find('.errtext').remove();
			$(section).find('*').removeClass('errorfield');
			// save input data! check if everything is alright.
			var url = fluxcessRsvpUrl + "booking/" + bookingId;
			var dtype = "PUT";
			var bookinglinepart = null;
			var tjson = null;
			var data = $(section).find('.addressform').serializeArray();
			$.ajax({
				type: dtype,
				url: url,
				data: data,
				success: function(data) {
					if (data.err && data.err.length > 0) {
						var frm = $(section).find('.addressform');
						$(frm).prepend('<div class="errors">Fehler: <ul class="errorlist"></ul></div>');
						for (i = 0; i < data.err.length; i++) {
							var note = data.err[i].msg;
							if (note == undefined || note == "") {
								note = data.err[i].msg;
							}
							if (data.err[i].field && $('input[name="' + data.err[i].field + '"]').size() > 0) {
								$('input[name="' + data.err[i].field + '"]').toggleClass('errorfield',
									true);
								$('input[name="' + data.err[i].field + '"]').before("<div class='errtext' id='err" + data.err[i].field + "'>" + note + "</div>");
							} else {
								$(frm).find('.errorlist').prepend("<li>" + note + "</li>");
							}
						}
					} else {
						// bookingId = data.data.booking_id;
						reloadShoppingCart(section);

						// go to summary
						var eventid = $(this).parentsUntil('[data-eventid]').parent().data('eventid');
						menuPointer(section, 3);
						$(section).find('.shoppingcarttable').hide();
						$(section).find('.shoppingcart').hide();
						$(section).find('.addresses').hide();
						$(section).find('.summary').show();
					}
				},
				dataType: 'json'
			});

		} else {
			alert("Invalid booking Id");
		}
		return false;
	});

/**
 * initiate the final checkout. (step 4)
 */
$(document).on('click',
	'.doCheckout',
	function() {
		var eventid = $(this).parentsUntil('[data-eventid]').parent().data('eventid');
		doCheckout(this, eventid);
	});

/**
 * show checkout
 */
$(document).on('click', '.restart', function() {
		restart(this);
	});

function menuPointer(section, stepno) {
	$(section).find('.step').removeClass('stepactive');
	$(section).find('.step' + stepno).addClass('stepactive');
}

/**
 * items into the shopping cart
 */
$(document).on('click',
	'.tocart',
	function() {
		var eventid = $(this).parentsUntil('[data-eventid]').parent().data('eventid');
		var ticketcategory_id = $(this).parentsUntil('[data-ticketcategory_id]').parent().data('ticketcategory_id');
		var ticketcategoryvalue_id = $(this).parentsUntil('[data-ticketcategoryvalue_id]').parent().data('ticketcategoryvalue_id');

		addToShoppingCart($(this).parentsUntil('[data-eventid]').parent(),
			eventid, ticketcategory_id, ticketcategoryvalue_id);
	});
function addToShoppingCart(section, eventid, ticketcategory,
ticketcategoryvalue) {
	var url = fluxcessRsvpUrl + "bookingline";
	var dtype = "POST";
	var bookinglinepart = null;
	var tjson = null;
	var data = null;

	bookinglinepart = [{
		ticketcategory_id: ticketcategory,
		ticketcategoryvalue_id: ticketcategoryvalue
	}];
	tjson = JSON.stringify(bookinglinepart, null, 2);
	data = {
		fevent_id: eventid,
		bookinglinepart: tjson
	};
	$.ajax({
		type: dtype,
		url: url,
		data: data,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				bookingId = data.data.booking_id;
				reloadShoppingCart(section);
			}
		},
		dataType: 'json'
	});
}

/**
 * remove items from shopping cart
 */
$(document).on('click',
	'.remove',
	function() {
		var eventid = $(this).parentsUntil('[data-eventid]').parent().data('eventid');
		var bookinglineId = $(this).parentsUntil('[data-bookingline_id]').parent().data('bookingline_id');

		removeFromShoppingCart($(this).parentsUntil('[data-eventid]').parent(), eventid, bookingId, bookinglineId, 0);
	});

function removeFromShoppingCart(section, eventid, bookingId, bookinglineId) {
	var url = fluxcessRsvpUrl + "bookingline/" + bookinglineId;
	var dtype = "DELETE";
	var data = {
		fevent_id: eventid,
		booking_id: bookingId,
		bookingline_id: bookinglineId
	};
	$.ajax({
		type: dtype,
		url: url,
		data: data,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				reloadShoppingCart(section);
			}
		},
		dataType: 'json'
	});
}

/**
 * get the shopping cart id / booking id from the server. section is the section
 * where the shopping cart is located on the html page.
 *
 * @param section
 */
function getBookingId(divid) {
	var url = fluxcessRsvpUrl + "booking/getBookingId";
	var dtype = "GET";
	$.ajax({
		type: dtype,
		url: url,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				bookingId = data.data.booking_id;
				reloadShoppingCart($('#' + divid));
			}
		},
		dataType: 'json'
	});
}
/**
 * reloads the shopping cart from the server.
 *
 * @param section
 */
function reloadShoppingCart(section) {
	var shoppingcart = null;
	if (bookingId == null) {
		$(section).find('.cart').html("Warenkorb leer.");
	} else {
		var url = fluxcessRsvpUrl + "booking/" + bookingId + "?special=full";
		var dtype = "GET";
		$.ajax({
			type: dtype,
			url: url,
			success: function(data) {
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].msg + " ";
					}
					alert(errtxt);
				} else {
					if (data.data.record.bookingstatus < 10) {
						fillShoppingCart(section, data);
						$(section).find('.summary').html(fillSummary(data, 'preview'));
					} else {
						$(section).find('.summary').html(fillSummary(data, 'preview'));
					}
				}
			},
			dataType: 'json'
		});
	}
}

function fillSummary(data, mode) {

	html = '';
	if (mode == 'final') {
		html += '<h2>Vielen Dank für Ihre Bestellung!</h2>';
		html += 'Bestellnummer: ' + data.data.record.booking_id + '<br />';
	} else {
		html += '<h2>Zusammenfassung - bitte prüfen Sie Ihre Daten!</h2>';
	}

	html += generateShoppingCartItemList(data, false);

	if (mode == 'preview') {
		html += '<a href="#" class="toCart">ändern...</a>';
	}
	html += 'Zahlungsweise: ' + data.data.record.paymenttype_id + '<br />';
	html += 'Lieferart: ' + data.data.record.shippingtype_id + '<br />';
	html += 'Ihre Daten: ' + generateAddressSummary(data) + '<br />';

	if (mode == 'preview') {
		html += '<input type="button" class="doCheckout" value="Jetzt kaufen!" />';
	}
	return html;
}

function generateAddressSummary(data) {
	var dt = data.data.record;
	html = '';
	html += '<h3>Adresse</h3>';
	html += dt.inv_salutation + " " + dt.inv_firstname + " " + dt.inv_lastname + "<br />";
	if (dt.inv_company != '') {
		html += dt.inv_company + '<br />';
	}
	html += dt.inv_street + ' ' + dt.inv_streetno + '<br />';
	html += dt.inv_zip + ' ' + dt.inv_city + '<br />';
	html += '<br />';
	html += 'E-Mail: ' + dt.inv_email;
	return html;
}

function generateShoppingCartItemList(data, editable) {
	html = '<table>';
	for (i = 0; i < data.data.record.bookingline.length; i++) {
		html += '<tr data-bookingline_id="' + data.data.record.bookingline[i].bookingline_id + '"><td>';
		var subtitle = "";
		for (j = 0; j < data.data.record.bookingline[i].bookinglinepart.length; j++) {
			subtitle += data.data.record.bookingline[i].bookinglinepart[j].title + " / ";
		}
		subtitle = subtitle.substring(0, (subtitle.length - 3));
		html += subtitle + '</td>';
		html += '<td>' + data.data.record.bookingline[i].pricegross + '</td>';
		if (editable == true) {
			html += '<td><input type="button" class="remove" value="löschen"></td>';
		}
		html += '</tr>';
	}
	html += '<tr><td>Summe</td><td>' + data.data.record.sumgross + '</td></tr>';
	html += '</table>';
	return html;
}

function fillShoppingCart(section, data) {
	html = 'Angebot #' + bookingId + generateShoppingCartItemList(data, true);
	if (data.data.record.bookingline.length > 0) {
		$(section).find('.cart').data('filled', "1");
	} else {
		$(section).find('.cart').data('filled', "0");
	}
	$(section).find('.cart').html(html);
	$(section).find('.paymenttype').find('[data-paymenttype_id="' + data.data.record.paymenttype_id + '"]').addClass("selected");
	$(section).find('.shippingtype').find('[data-shippingtype_id="' + data.data.record.shippingtype_id + '"]').addClass("selected");
	$.each(data.data.record, function(index, element) {
			try {
				$(section).find('input[name="' + index + '"]').val(element);
			} catch (e) {
				alert(e);
			}
		});

}
function parseTicketCategories(element, data) {
	var divid = $(element).attr('id');
	ticketcategories[divid] = [];
	for (i = 0; i < data.data.ticketcategory.length; i++) {
		var tc = data.data.ticketcategory[i];
		ticketcategories[divid][tc.ticketcategorycode] = tc;
	}
}
function generateBookingForm(element, data) {
	try {
		var divid = $(element).attr('id');
		var guestData = null;
		var d = data.data;
		var i = 0;
		var ix = 0;
		var ii = 0;
		var iix = 0;
		var iii = 0;
		var iiix = 0;
		var ph = "";
		menuHtml = "";
		try {
			if (data.session.loggedin == undefined) {
				data.session.loggedin = "0";
			}
		} catch (err) {
			data.session.loggedin = "0";
		}
		
		if (d.selfregistrationallowed == "0") {
			guestData = flGuestData[divid];
		}

		if (d.selfregistrationallowed == "0" && data.session.loggedin == "0") {
			ph += '<form class="fluxcess_loginform" method="post">';
			ph += '<div class="fluxcess_errorMessage"></div>';
			ph += '<input name="username" type="username" placeholder="Last Name" autofocus />';
			ph += '<input name="password" type="password" placeholder="Access Code" />';
			ph += '<div class="fluxcess_loadingMessage"></div>';
			ph += '<div class="fluxcess_waitMessage"></div>';
			ph += '<input type="submit" value="Login" />';
			ph += '</form>';
		} else {
			ph += '<form class="bookingform" method="post">';
			if (!d.hasOwnProperty("pages")) {
				throw new Error('No pages created.');
			}
			var numberOfPages = Object.keys(d.pages).length;
			// for (i = 0; i < d.pages.length; i++) {
			for (var ix = 0; ix < numberOfPages; ix++) {
				i = Object.keys(d.pages)[ix];
				// alert(d.pages[i]);
				if (d.pages[i] != null) {

					/* Generate forward / back buttons */
					var bbar = '<div class="bbar">';
					if (ix > 0 && d.pages[i].backallowed != 0) {
						bbar += '<button type="button" class="navbutton backbutton btn">zurück</button>';
					}
					if (ix < numberOfPages - 1 && d.pages[i].buynow != 1 && d.pages[i].checkout != 1) {
						bbar += '<button type="button" class="navbutton nextbutton btn btn-signal">weiter ...</button>';
						bbar += '<input type="submit" class="hidden" />';
					} else if (d.pages[i].buynow) {
						bbar += '<button type="button" class="navbutton nextbutton btn btn-signal buynow">' + d.pages[i].buynow + '</button>';
					}
					bbar += '</div>';

					/* Generate menu bar */
					menuHtml += '<td class="step step' + (ix * 1 + 1);
					if (ix == 0) {
						menuHtml += ' stepactive';
					}
					if (d.pages[i].directaccess == 0) {
						menuHtml += ' nodirectaccess';
					}
					menuHtml += '" data-step="' + (ix + 1) + '">' + d.pages[i].pagetitle + '</td>';

					ph += '<div class="bpage';
					if (ix == 0) {
						ph += ' active';
					}
					ph += ' bpage_' + (ix + 1) + '">';
					if ( (d.buttonstop && d.buttonstop != 0) || (d.buttonstop && d.buttonstop == 1)) {
						ph += bbar;
					}
					ph += '<div class="bookingerrorfield"></div>';

					var numberOfSections = 0;
					if (d.pages[i].sections != null) {
						numberOfSections = Object.keys(d.pages[i].sections).length;
					}
					for (iix = 0; iix < numberOfSections; iix++) {
						console.log(d.pages[i].sections);
						ii = Object.keys(d.pages[i].sections)[iix];
						var s = d.pages[i].sections[ii];
						var maxNumberOfGuests = 1;
						var numberOfGuestsInData = guestData.length;
						if (s.perguest && s.perguest == 1) {
							maxNumberOfGuests = guestData[0].invited;
						}
						for (var u = 0; u < maxNumberOfGuests; u++) {
							if (u < numberOfGuestsInData) {
								ph += '<div class="fluxcess_perGuestForm" data-guestActive="1">';
							} else {
								ph += '<div class="fluxcess_perGuestForm hidden" data-guestActive="0">';
							}
							if (s.type == 'tcatchoice') {
								try {
									var preselected = "";
									if (s.ticketchoice == null) {
										s.ticketchoice = s.ticketcategorylist;
									}
									for (iii = 0; iii < s.ticketchoice.length; iii++) {
										var tcatid = s.ticketchoice[iii];
										var tc = ticketcategories[divid][tcatid];
										if (tc != undefined && tc.preselected == 1) {
											preselected += tcatid + ",";
										}
									}
									var maximumchoices = 1;
									if (s.maximumchoices) {
										maximumchoices = s.maximumchoices;
									}
									ph += '<input type="hidden" name="' + s.name + '" value="' + preselected + '" />';
									if (s.display != -1) {
										ph += '<ul class="tcatchoice" data-maximumchoices="' + maximumchoices + '">';
										for (iii = 0; iii < s.ticketchoice.length; iii++) {
											var tcatid = s.ticketchoice[iii];
											var tc = ticketcategories[divid][tcatid];
											if (tc) {
												ph += '<li data-catid="' + tcatid + '" data-name="' + s.name + '" class="';
												if (tc.preselected == 1) {
													ph += 'active';
												}
												ph += '">' + tc.title + ' (' + tc.price + ' &euro;)';
												if (tc.desc) {
													ph += '<p class="ticketcategorydescription">' + tc.desc + '</p>';
												}
												ph += '</li>';
											} /*
											* else { ph += '<li>' + tcatid + '</li>'; }
											*/
										}
										ph += '</ul>';
									}
								} catch (err) {
									alert("Fehler beim Erstellen eines tcatchoice Bereiches: " + err);
								}
							} else if (s.type == 'fields') {
								try {
									if (s.groupname) {
										ph += '<div class="fieldgroup_' + s.groupname;
										if (s.display && s.display == 'hidden') {
											ph += ' ticketbookinghidden';
										}
										ph += '">';
									}
									var thisGuestData = guestData[u];
									if (s.fields) {
										var numberOfFields = Object.keys(s.fields).length;
										for (iiix = 0; iiix < numberOfFields; iiix++) {
											iii = Object.keys(s.fields)[iiix];
											ph += generateFieldHtml(s.fields[iii], thisGuestData);
										}
									}
									if (s.groupname) {
										ph += '</div>';
									}
								} catch (err) {
									alert("Fehler beim Generieren eines fields Objektes: " + err);
								}
							} else if (s.type == 'sessionchoice') {
								if (s.sessions) {
									ph += generateSessionchoiceHtml(s);
								} else {
									alert("Fehler: sessions sind leer (" + s + ")");
								}
							} else if (s.type == 'loginchoice') {
								ph += generateLoginchoiceHtml(s);
							} else if (s.type == 'summary') {
								ph += generateSummaryHtml(s);
							}
							if (u > 0) {
								ph += '<div class="right"><button class="fluxcess_remove_guest">Remove guest</button></div>';
							}
							ph += '</div>';
						}
						if (s.perguest && s.perguest == 1) {
							maxNumberOfGuests = guestData[0].invited;
							if (numberOfGuestsInData < maxNumberOfGuests) {
								ph += 'Your invitation is valid for ' + maxNumberOfGuests + ' persons.';
								ph += '<button class="fluxcess_add_guest">Add one person</button>';
							}
						}
					}
					ph += bbar + '</div>';
				}
			}
			/*
			 * html += '<td class="step step2 toCheckout">2 - Kasse</td>'; html += '<td class="step step3 toSummary">3 -
			 * Zusammenfassung</td>'; html += '<td class="step step4">4 -
			 * Buchung</td>';
			 */
			ph += '</form>';
		}
	} catch (err) {
		var errDiv = '<div class="fluxcess_technical_error">Technical problem with the booking form:<br /><br />';
		errDiv += err + '<br /><br />';
		errDiv += '<button onClick="reloadBookingForm($(findCurrentBookingElement($(this))));">Retry...</button></div>';
		$(element).find('.ticketleft').html('').append(errDiv);
	} finally {
		$(element).find('.stepmenu tr').html(menuHtml);
		$(element).find('.ticketleft').append(ph)
	}
	// $(element).find('.eventtitle').html(': ' + d.eventtitle);
	$(element).find('.publicshopheadline').html(d.publicshopheadline);
}

/**
 * Reloads the entire form via ajax / refreshes its content.
 * Can be used for a "reload" button for the form.
 * @param element 
 */
function reloadBookingForm(element) {
	$(element).find('.fluxcess_technical_error').remove();
	loadGuestData(element, eventid);
	loadFormSequence(element, $(element).data('eventid'));
}

/**
 * returns the current booking element, depending on the given element.
 * @param {*} element 
 */
function findCurrentBookingElement(element) {
	return $(element).parentsUntil('.ticketbooking').parent();
}

/**
 * returns the uid of the current booking element.
 * Booking element needs to be given.
 */
function getCurrentBookingElementUid(element) {
	return $(element).attr('id');
}

function generateSummaryHtml(f) {
	var html = '';
	html += '<div class="bookingsummary">smm</div>';
	return html;
}
function generateLoginchoiceHtml(f) {
	var html = '';
	if (f.logins != null) {
		for (i = 0; i < f.logins.length; i++) {
			var l = f.logins[i];
			if (l.type == 'nologin') {
				html += '<label>E-Mail Adresse <input name="emailaddress" type="email" placeholder="info@example.org" /></label>';
			} else if (l.type == 'guest') {
				html += '<div class="hidepart">';
				html += '<div class="foldhead">' + l.title + '</div>';
				html += '<div class="foldcontent';
				if (l.preselected == undefined || l.preselected == "0") {
					html += ' hidden';
				}
				html += '"><label>E-Mail Adresse <input name="emailaddress" type="email" placeholder="info@example.org" /></label>';
				html += '<label><input type="checkbox" name="register" value="1" /> Ich möchte meine Daten für spätere Registrierungen speichern.</label>';
				html += '</div>';
				html += '</div>';
			} else if (l.type == 'alldata') {
				html += '<div class="hidepart">';
				html += '<div class="foldhead">' + l.title + '</div>';
				html += '<div class="foldcontent';
				if (l.preselected == undefined || l.preselected == "0") {
					html += ' hidden';
				}
				html += '">Bitte loggen Sie sich zunächst ein und kommen dann hierher zurück.';
				html += '<a href="https://example.org/login/">Zum Login</a></div>';
				html += '</div>';
			} else if (l.type == 'internal') {
				html += '<div class="hidepart">';
				html += '<div class="foldhead">' + l.title + '</div>';
				html += '<div class="foldcontent';
				if (l.preselected == undefined || l.preselected == "0") {
					html += ' hidden';
				}
				html += '"><input type="email" name="emailaddress2" />';
				html += '<input type="password" name="password" />';
				html += '<input type="submit" value="Login" /></div>';
				html += '</div>';
			}
		}
	} else {
		html += '<label>E-Mail Adresse <input name="emailaddress" type="email" placeholder="info@example.org" /></label>';
	}
	return html;
}
/**
 * returns a matrix of the sessions of the format sessionmatrix[dmyhm][stream] =
 * sessiondata-array
 *
 * @param eventSessions //
 *            sessions of the event in the form of an object
 * @returns {}
 */
function getSessionMatrix(streams, eventSessions) {
	// var streams = ['F1', 'F2', 'F3', 'F4', 'F5'];
	var sessionmatrix = { };
	for (i = 0; i < eventSessions.sessions.length; i++) {
		var dmyhm = eventSessions.sessions[i].datetime;
		// stream
		for (var j = 0; j < eventSessions.sessions[i].stream.length; j++) {
			// alert("clear " + j.toString() + " of " +
			// eventSessions.sessions[i].stream.length);
			if (!sessionmatrix[dmyhm]) {
				sessionmatrix[dmyhm] = { };
			}
			var stream = eventSessions.sessions[i].stream[j];
			// alert(stream);
			// check for a multi-column field
			var i1 = 1;
			var columnCounter = 1;
			// alert("I am ("+ ((j+i1).toString())+") " +
			// eventSessions.sessions[i].title + streams[j+i1] +
			// eventSessions.sessions[i].stream[j+i1]);//[streams[j+i1]]);
			while (eventSessions.sessions[i].stream[j + i1] != undefined) { // ==
				// streams[j+i1])
				// {
				// alert("noch eins " + streams[j+i1]);
				i1 += 1;
				columnCounter += 1;
			}
			eventSessions.sessions[i].cols = i1;
			sessionmatrix[dmyhm][stream] = eventSessions.sessions[i];
			j += columnCounter - 1;
		}
	}
	return sessionmatrix;
}
function generateSessionchoiceHtml(eventSessions) {
	var streams = [];// ['F1', 'F2', 'F3', 'F4', 'F5'];
	var html = '';

	var header = '<tr><th';
	if (eventSessions.thbackgroundcolor || eventSessions.thcolor) {
		header += ' style="';
		if (eventSessions.thbackgroundcolor) {
			header += 'background-color:' + eventSessions.thbackgroundcolor + ";";
		}
		if (eventSessions.thcolor) {
			header += 'color:' + eventSessions.thcolor + ";";
		}
		header += '"';
	}
	header += '></th>';

	for (var i = 0; i < eventSessions.streams.length; i++) {
		header += '<th';
		if (eventSessions.thbackgroundcolor || eventSessions.thcolor) {
			header += ' style="';
			if (eventSessions.thbackgroundcolor) {
				header += 'background-color:' + eventSessions.thbackgroundcolor + ";";
			}
			if (eventSessions.thcolor) {
				header += 'color:' + eventSessions.thcolor + ";";
			}
			header += '"';
		}
		header += '>' + eventSessions.streams[i].title + '</th>';
		streams[streams.length] = eventSessions.streams[i].id;
	}
	header += '</tr>';

	var sessionmatrix = getSessionMatrix(streams, eventSessions);
	var sessionlayout = eventSessions.sessionlayout;
	if (sessionlayout == undefined) {
		sessionlayout = 1;
	}
	html = '<input name="' + eventSessions.name + '" value="" type="hidden" />';
	html += '<table class="sessionchoice sessionlayout' + sessionlayout + '" data-selectionname="' + eventSessions.name + '"';
	if (eventSessions.maxwidth) {
		html += ' style="max-width:' + eventSessions.maxwidth + ';margin:auto;" ';
	}

	html += '>';
	html += header;

	var stable = '';
	// time slot
	var breakline = false;
	var pastheadline = true;
	var headline = false;
	for (i = 0; i < Object.keys(sessionmatrix).length; i++) {
		// session slot
		var ts = Object.keys(sessionmatrix)[i];
		var lefthead = '';
		var restrow = '';
		var pastbreakline = breakline;
		breakline = false;

		for (var j = 1; j < Object.keys(eventSessions.streams).length + 1; j++) {
			var data = undefined;
			var streamname = Object.keys(eventSessions.streams)[j];
			if (sessionmatrix[ts][streams[j - 1]]) {
				data = sessionmatrix[ts][streams[j - 1]];
			} else if (sessionmatrix[ts][j]) {
				data = sessionmatrix[ts][j];
			}
			// alert(j.toString() + streams[j-1] + " " +
			// sessionmatrix[ts][streams[j-1]]);
			if (data && (eventSessions.sessionlayout == undefined || eventSessions.sessionlayout == 1)) {
				restrow += '<td class="sessionselect ' + data.type + '" colspan="' + data.cols + '" data-sessionid="' + data.id + '">';
				restrow += '<div class="title">';
				if (data.lefticon) {
					restrow += '<img src="' + data.lefticon + '" alt="' + data.title + '"/> ';
				}
				restrow += data.title + '</div>';
				restrow += '<div class="speaker">' + data.speaker + '</div>';
				restrow += '<div class="description">' + data.descr + '</div>';
				restrow += '</td>';
				if (data.cols > 1) {
					j += data.cols - 1;
				}
				if (data.cols > 0 && data.type == 'break') {
					breakline = true;
				}
			} else if (data && (eventSessions.sessionlayout != undefined && eventSessions.sessionlayout == 2)) {
				// alert(data.cols);
				var fullybooked = 0;
				if (data.fullybooked) {
					if (data.fullybooked == 1) {
						fullybooked = 1;
					}
				}
				restrow += '<td class="sessionselect ' + data.type;
				if (fullybooked == 1) {
					restrow += ' fullybooked';
				}
				restrow += '" colspan="' + data.cols + '" data-sessionid="' + data.id + '"';
				if (data.linked !== undefined) {
					restrow += ' data-linked="';
					var shorten = false;
					for (jj = 0; jj < data.linked.length; jj++) {
						restrow += data.linked[jj] + ', ';
						shorten = true;
					}
					if (shorten == true) {
						restrow = restrow.substring(0, restrow.length - 2);
					}
					restrow += '"';
				}
				restrow += ' data-fullybooked="' + fullybooked + '"';
				restrow += '>';
				if (data.no) {
					restrow += '<div class="sessionno">' + data.no + ' | <span class="infolink">Info</span></div>';
				}
				restrow += '<div class="speaker">' + data.speaker + '</div>';
				restrow += '<div class="datetime">' + data.datetime + '</div>';
				restrow += '<div class="datetimeto">' + data.datetimeto + '</div>';
				restrow += '<div class="speakerpic">' + data.speakerpic + '</div>';
				restrow += '<div class="sessionnoh hidden">' + data.no + '</div>';
				restrow += '<div class="title">';
				if (data.lefticon) {
					restrow += '<img src="' + data.lefticon + '" alt="' + data.title + '"/> ';
				}
				restrow += data.title + '</div>';

				restrow += '<div class="description">' + data.descr + '</div>';
				restrow += '</td>';

				if (data.cols > 1) {
					j += data.cols - 1;
				}
				if (data.cols > 0 && data.type == 'break') {
					breakline = true;
				}
			} else {
				restrow += '<td>-</td>';
			}
		}
		lefthead += '<tr class="';
		if (breakline == true) {
			lefthead += ' breakline ';
		}
		if (pastbreakline == true) {
			lefthead += ' pastbreakline ';
		}
		if (pastheadline == true) {
			lefthead += ' pastheadline ';
		}
		lefthead += '"><td class="stime">';
		lefthead += toreadablets(ts, false);
		lefthead += '</td>';
		pastheadline = false;
		stable += lefthead + restrow + '</tr>';
	}

	html += stable;
	html += '</table>';
	return html;
}
function toreadablets(datetime, withspan) {
	var yr = datetime.substr(0, 4);
	var mon = datetime.substr(4, 2);
	var day = datetime.substr(6, 2);
	var hr = datetime.substr(8, 2);
	var min = datetime.substr(10, 2);
	// var dt = '<span class="date">' + day + "." + mon + "." + yr + '</span><br
	// /><span class="time">' + hr + ":" + min + '</span>';
	var dt = '';
	if (withspan == true) {
		dt = '<span class="time">' + hr + ":" + min + '</span>';
	} else {
		dt = hr + ":" + min;
	}
	return dt;
}

function generateFieldHtml(f, thisGuestData) {
	var ph = '';
	switch (f.type) {
	case 'checkbox':
		ph += fGenerateCheckbox(f);
		break;
	case 'html':
		ph += fGenerateHtml(f);
		break;
	case 'input':
		ph += fGenerateInput(f, thisGuestData);
		break;
	case 'radio':
		ph += fGenerateRadio(f);
		break;
	case 'select':
		ph += fGenerateSelect(f);
		break;
	case 'textarea':
		ph += fGenerateTextarea(f);
		break;
	}
	return ph;
}

function fGenerateSelect(f) {
	var ph = '';
	if (f.caption) {
		ph += '<label for="' + f.name + '">' + f.caption + '</label>';
	}
	ph += '<select name="' + f.name + '">';
	for (i = 0; i < f.options.length; i++) {
		ph += '<option value="' + f.options[i].value + '">' + f.options[i].title + '</option>';
	}
	ph += '</select>';
	return ph;
}

function fGenerateInput(f, thisGuestData) {
	var ph = '';
	if (f.caption) {
		ph += '<label for="' + f.name + '">' + f.caption + '</label>';
	}
	ph += '<input name="' + f.name + '" ';
	if (f.placeholder) {
		ph += ' placeholder="' + f.placeholder + '" ';
	}
	if (thisGuestData && thisGuestData[f.name]) {
		ph += ' value="' + thisGuestData[f.name] + '" ';
	}
	ph += ' />';
	return ph;
}
function fGenerateTextarea(f) {
	var ph = '';
	if (f.caption) {
		ph += '<label for="' + f.name + '">' + f.caption + '</label>';
	}
	ph += '<textarea name="' + f.name + '" ';
	if (f.placeholder) {
		ph += ' placeholder="' + f.placeholder + '" ';
	}
	ph += '></textarea>';
	return ph;
}
function fGenerateRadio(f) {
	var ph = '';
	if (f.nomargin) {

	} else {
		ph += '<div class="fieldarea">';
	}
	if (f.caption != null) {
		ph += '<div class="caption">' + f.caption + '</div>';
	}
	if (f.choices == undefined) {
		f.choices = f.options;
	}
	for (i = 0; i < f.choices.length; i++) {
		var myVal = f.choices[i].title;
		if (f.choices[i].val) {
			myVal = f.choices[i].val;
		}
		var hideshowgroups = '';
		if (f.choices[i].showsectionlist) {
			for (ii = 0; ii < Object.keys(f.choices[i].showsectionlist).length; ii++) {
				var groupname = f.choices[i].showsectionlist[ii];// Object.keys(f.choices[i].showsectionlist)[ii];

				var showhide = 1;
				hideshowgroups += groupname + ":" + showhide + ",";
			}
			hideshowgroups = hideshowgroups.substring(0,
				hideshowgroups.length - 1);
		}
		if (f.choices[i].hidesectionlist) {
			for (ii = 0; ii < Object.keys(f.choices[i].hidesectionlist).length; ii++) {
				var groupname = f.choices[i].hidesectionlist[ii];// Object.keys(f.choices[i].showsectionlist)[ii];

				var showhide = 0;
				hideshowgroups += groupname + ":" + showhide + ",";
			}
			if (hideshowgroups.substring(hideshowgroups.length - 1) == ',') {
				hideshowgroups = hideshowgroups.substring(0,
					hideshowgroups.length - 1);
			}
		}
		ph += '<label>';
		ph += '<input type="radio" name="' + f.name + '" value="' + myVal + '" ';
		if (hideshowgroups.length > 0) {
			ph += ' class="hideshow" ';
			ph += ' data-hideshow="' + hideshowgroups + '" ';
		}
		ph += ' /> ';
		ph += f.choices[i].title;

		ph += '</label>';
	}
	if (f.nomargin) {

	} else {
		ph += '</div>';
	}
	return ph;
}
function fGenerateCheckbox(f) {
	var ph = '';
	if (f.nomargin) {

	} else {
		ph += '<div class="fieldarea">';
	}
	if (f.caption != null) {
		ph += '<div class="caption">' + f.caption + '</div>';
	}
	if (f.choices == undefined) {
		f.choices = f.options;
	}
	for (i = 0; i < f.choices.length; i++) {
		var val = f.choices[i].value;
		if (val == undefined) {
			val = f.choices[i].title;
		}
		ph += '<label>';
		ph += '<input type="checkbox" name="' + f.name + '[]" value="' + val + '" /> ';
		ph += f.choices[i].title;
		ph += '</label>';
	}
	if (f.nomargin) {

	} else {
		ph += '</div>';
	}
	return ph;
}
function fGenerateHtml(f) {
	var ph = '';
	if (f.content) {
		ph += f.content;
	}
	return ph;
}

/**
 *
 */
$(document).on('click',
	'.ticketleft .navbutton',
	function() {
		var nextPage = 0;
		var dontGo = false;
		var currentPageId = $(this).parentsUntil('.ticketbooking').parent().data('pageid');
		var eventid = $(this).parentsUntil('.ticketbooking').parent().data('eventid');
		var divid = $(this).parentsUntil('.ticketshop').parent().attr('id');
		if (currentPageId == undefined) {
			currentPageId = 1;
		}
		if ($(this).hasClass('nextbutton')) {
			var special = '';
			nextPage = currentPageId + 1;
			if ($(this).parentsUntil('.ticketbooking').parent().find('.bpage_' + nextPage).length == 0) {
				nextPage = currentPageId;
			}
			if ($(this).hasClass('buynow')) {
				special = 'buynow';
			}
			gotoNextPage(currentPageId, nextPage, divid, eventid,
				special);
			dontGo = true;
		} else {

			nextPage = currentPageId - 1;
			if (nextPage < 1) {
				nextPage = 1;
			}
		}
		if (dontGo == false) {
			$(this).parentsUntil('.ticketbooking').parent().data('pageid', nextPage);
			var destination = "ticketshop/" + divid + "/" + eventid + "/" + nextPage;
			document.location.hash = destination;
		} else {
			$('#' + divid)[0].scrollIntoView(true);
		}
	});
function gotoNextPage(currentPageId, nextPage, divid, eventid, special) {
	var dontGo = false;
	if (nextPage > currentPageId) {
		checkPage(divid, eventid, currentPageId, nextPage, special)
		dontGo = true;

		if (dontGo == false) {
			$(this).parentsUntil('.ticketbooking').parent().data('pageid',
				nextPage);
			var destination = "ticketshop/" + divid + "/" + eventid + "/" + nextPage;
			document.location.hash = destination;
		}
	}
	if (dontGo == true) {
		menuPointer($('#' + divid), currentPageId);
	} else {
		menuPointer($('#' + divid), nextPage);
	}

}
function checkPage(divid, eventid, currentPageId, nextPage, special) {
	/*
	 * if (bookingId[divid] != null) { }
	 */
	var url = fluxcessRsvpUrl + "booking/0/check";// + bookingId[divid] +
	// "/checkout";
	var dtype = "POST";
	var data = $('#' + divid).find('.bookingform').serializeArray();
	data.push({
		name: 'divid',
		value: divid
	});
	data.push({
		name: 'fevent_id',
		value: eventid
	});
	data.push({
		name: 'currentpage',
		value: currentPageId
	});
	data.push({
		name: 'nextpage',
		value: nextPage
	});
	data.push({
		name: 'special',
		value: special
	});
	$('#' + divid).find('.bookingerror').remove();
	$('.bookingerrormain').remove();
	$.ajax({
		type: dtype,
		url: url,
		data: data,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				writeErrorMessages(data.err);
			} else {
				$('#' + divid + ' .bookingsummary').html(data.data.summary);
				if (data.data.checkout == 1) {

					$('#' + divid + ' .ticketbooking').data('pageid',
						nextPage);
					var destination = "ticketshop/" + divid + "/" + eventid + "/" + nextPage;
					document.location.hash = destination;

					// showSummary(section, data, 'bought');
					// clearShoppingCart(section);
					// bookingId = null;
				} else {
					$('#' + divid + ' .ticketbooking').data('pageid',
						nextPage);
					var destination = "ticketshop/" + divid + "/" + eventid + "/" + nextPage;
					document.location.hash = destination;
				}
			}
		},
		dataType: 'json'
	});/*
	 * } else { alert("BuchungsId ungültig."); }
	 */
	/* if everything is correct, no errors, go to next page: */
	// $(this).parentsUntil('.ticketbooking').parent().data('pageid', nextPage);
	// var destination = "ticketshop/" + divid+"/"+eventid + "/"+nextPage;
	// document.location.hash = destination;
	// alert(destination);
}
function writeErrorMessages(e) {
	var errtxt = '';
	var generaltxt = '';
	for (i = 0; i < e.length; i++) {
		// get the field type
		// react, depending on the field type.
		if (e[i].field == '') {
			generaltxt += '<li>' + e[i].msg + "</li>";
		} else if ($('[name="' + e[i].field + '"]').attr('type') == 'radio') {
			$('[name="' + e[i].field + '"]:first').before('<span class="bookingerror">' + e[i].msg + '</span>');
		} else if ($('[name="' + e[i].field + '"]').length > 0) {
			$('[name="' + e[i].field + '"]').before('<span class="bookingerror">' + e[i].msg + '</span>');
		} else if ($('[name="' + e[i].field + '[]"]').length > 0) {
			$('[name="' + e[i].field + '[]"]:first').before('<span class="bookingerror">' + e[i].msg + '</span>');
		}
		if (e[i].commontext) {
			errtxt += e[i].commontext;
		}
	}
	$('.bpage .bookingerrorfield').html('<div class="bookingerrormain">' + errtxt + '<ul>' + generaltxt + '</ul></div>');
}
/**
 * called when the user selects an event: loads the booking form for the event.
 *
 * @param element
 * @param eventid
 */
function goBooking(element, eventid) {
	loadFormSequence(element, eventid);
	html = '<div class="ticketbooking" data-eventid="' + eventid + '"><h2><span class="publicshopheadline"></span><span class="eventtitle"></span></h2><table><tr><td class="ticketleft">';

	html += '<table class="stepmenu"><tr>';
	html += '<td class="step step1 stepactive toCart">1 - Ticket Auswahl</td>';
	html += '<td class="step step2 toCheckout">2 - Kasse</td>';
	html += '<td class="step step3 toSummary">3 - Zusammenfassung</td>';
	html += '<td class="step step4">4 - Buchung</td>';
	html += '</tr></table>';

	html += '<div class="choice"></div>';
	html += '<div class="addresses"><h2>Kasse - Ihre Daten</h2>' + printAddressForm('inv') + '</div>';
	html += '<div class="summary"><h2>Zusammenfassung</h2></div>';
	html += '<div class="final"><h2>Ihre Bestellung</h2></div>';
	html += '</td>';
	html += '<td class="ticketright">';
	html += '<div class="shoppingcart"><h2>Warenkorb</h2><div class="cart" data-filled="0"></div>';
	html += '<h3>Zahlungsweise</h3><div class="paymenttype"></div>';
	html += '<h3>Lieferung</h3><div class="shippingtype"></div>';
	html += '<input type="button" value="zur Kasse..." class="toCheckout btn" />';

	html += '</div>';
	html += '</td></tr></table>';

	html += '</div>';

	$(element).find('.eventcatalogue').hide();
	$(element).append(html);
}

function loadGuestData(element, eventid, bookingFormData) {
	var url = fluxcessRsvpUrl + "fevent/" + eventid + "/guest?FLUXCESS="+fluxcessSessionId;
	var dtype = "GET";
	var elementId = getCurrentBookingElementUid(element);
	flGuestDataLoaded[elementId] = false;
	$.ajax({
		type: dtype,
		url: url,
		headers: {
			/**
			 * The fluxcess header contains the session id from the fluxcess rsvp server.
			 * Unfortunately, it is not allowed to set the 'cookie' header,
			 * and the session is not stored automatically by the browser somehow.
			 */
			'fluxcess': fluxcessSessionId 
		},
		success: function(data) {
			
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].msg + " ";
					}
					alert(errtxt);
				} else {
					flGuestDataLoaded[elementId] = true;
					flGuestData[elementId] = data.data.guests;

					//
					generateBookingForm(element, bookingFormData);
					$(element).find('.loading').remove();
				}
			
		},
		dataType: 'json'
	});
}

function loadFormSequence(element, eventid) {
	var url = fluxcessRsvpUrl + "fevent/" + eventid + "/formsequence?FLUXCESS="+fluxcessSessionId;
	var dtype = "GET";
	$.ajax({
		type: dtype,
		url: url,
		headers: {
			/**
			 * The fluxcess header contains the session id from the fluxcess rsvp server.
			 * Unfortunately, it is not allowed to set the 'cookie' header,
			 * and the session is not stored automatically by the browser somehow.
			 */
			'fluxcess': fluxcessSessionId 
		},
		success: function(data) {
			if (data == null) {
				$(element).find('.ticketleft').append('Event ' + eventid + ' konnte nicht geladen werden: Die Antwort des Servers war leer.')
			} else {
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].msg + " ";
					}
					alert(errtxt);
				} else {
					// this could be first contact with the server. session id needs to be stored in a cookie.
					fluxcessSessionId = data.session.sid;
					setCookie('fluxcessSessionId', fluxcessSessionId, 10);

					parseTicketCategories(element, data);

					/**
					 * If this is an RVSP form, the guest data needs to be loaded.
					 * If this is a registration form, no guest data is necessary. The form can be displayed.
					 */
					if (data.data.selfregistrationallowed == 0) {
						loadGuestData(element, eventid, data);
					} else {
						generateBookingForm(element, data);
						$(element).find('.loading').remove();
					}
				}
			}
		},
		dataType: 'json'
	});

}

/**
 * loads the choices for an event (ticket categories, ...)
 *
 * @param element
 * @param eventid
 * @param ticketcategory_id
 */
function loadChoices(element, eventid, ticketcategory_id) {
	var url = fluxcessRsvpUrl + "ticketcategory/" + ticketcategory_id + "/ticketcategoryvalue";
	var dtype = "GET";
	$.ajax({
		type: dtype,
		url: url,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				var html = "<table>";
				for (i = 0; i < data.data.records.length; i++) {
					var ticketcategoryvalue_id = data.data.records[i].ticketcategoryvalue_id;
					html += '<tr data-ticketcategoryvalue_id="' + ticketcategoryvalue_id + '"><td>';
					html += data.data.records[i].title + "</td>";
					html += '<td>' + data.data.records[i].price + "</td>";
					html += '<td><input type="button" value="in den Warenkorb" class="tocart" /></td>';
					html += '</tr>';
				}
				html += "</table>";
				$(element).parent().parent().find('[data-ticketcategory_id="' + ticketcategory_id + '"]').append(html);
			}
		},
		dataType: 'json'
	});
}

function goCheckout(element, event_id) {
	$(element).parentsUntil('[data-eventid]').parent().find('.choice').hide();
	/*
	 * var html = '<td class="checkout"><h2>Kasse</h2>'; // html +=
	 * showSummary(element, data, 'prebuyconfirm'); html += '<input
	 * type="button" class="doCheckout" value="Jetzt kaufen!" />'; html += '</td>';
	 * $(element).parentsUntil('table').find('> tr').append(html);
	 */
}

function doCheckout(divid, event_id) {
	// if (bookingId[divid] != null) {
	var url = fluxcessRsvpUrl + "booking/0/checkout";
	var dtype = "POST";
	$.ajax({
		type: dtype,
		url: url,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				if (data.data.checkout == 1) {
					showSummary(section, data, 'bought');
					clearShoppingCart(section);
					bookingId = null;
				} else {
					alert("Fehler");
				}
			}
		},
		dataType: 'json'
	});
	/*
	 * } else { alert("BuchungsId ungültig."); }
	 */
}

function selectPaymenttype(section, paymenttype_id) {
	if (bookingId != null) {
		var url = fluxcessRsvpUrl + "booking/" + bookingId;
		var dtype = "PUT";
		var data = {
			paymenttype_id: paymenttype_id
		};
		$.ajax({
			type: dtype,
			url: url,
			data: data,
			success: function(data) {
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].msg + " ";
					}
					alert(errtxt);
				} else {
					$(section).parent().find('li').removeClass('selected');
					$(section).addClass('selected');
				}
			},
			dataType: 'json'
		});
	} else {
		alert("BuchungsId ungültig.");
	}
}
function selectShippingtype(section, shippingtype_id) {
	if (bookingId != null) {
		var url = fluxcessRsvpUrl + "booking/" + bookingId;
		var dtype = "PUT";
		var data = {
			shippingtype_id: shippingtype_id
		};
		$.ajax({
			type: dtype,
			url: url,
			data: data,
			success: function(data) {
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].msg + " ";
					}
					alert(errtxt);
				} else {
					$(section).parent().find('li').removeClass('selected');
					$(section).addClass('selected');
				}
			},
			dataType: 'json'
		});
	} else {
		alert("BuchungsId ungültig.");
	}
}

function showSummary(section, data, mode) {
	var html = fillSummary(data, 'final');
	html += '<a class="restart" href="#">Neustart</a>';
	$(section).parentsUntil('.ticketbooking').parent().find('.summary').hide();
	$(section).parentsUntil('.ticketbooking').parent().find('.final').html(html);
	$(section).parentsUntil('.ticketbooking').parent().find('.final').show();
}
function clearShoppingCart(section) { }
/**
 * remove the buying process, show the catalogue
 *
 * @param section
 */
function restart(section) {
	$(section).parentsUntil('.ticketshop').parent().find('.eventcatalogue').show();
	$(section).parentsUntil('.ticketshop').remove();
}

function printAddressForm(type) {
	html = '<form class="addressform">';
	html += '<label for="' + type + '_salutation">Anrede</label><input name="' + type + '_salutation" />';
	html += '<label for="' + type + '_firstname">Vorname</label><input name="' + type + '_firstname" />';
	html += '<label for="' + type + '_lastname">Nachname</label><input name="' + type + '_lastname" />';
	html += '<label for="' + type + '_company">Firma</label><input name="' + type + '_company" />';
	html += '<label for="' + type + '_street">Straße</label><input name="' + type + '_street" />';
	html += '<label for="' + type + '_streetno">Hausnr.</label><input name="' + type + '_streetno" />';
	html += '<label for="' + type + '_zip">PLZ</label><input name="' + type + '_zip" />';
	html += '<label for="' + type + '_city">Ort</label><input name="' + type + '_city" />';
	html += '<br />';
	html += '<label for="' + type + '_email">E-Mail Adresse</label><input name="' + type + '_email" />';
	html += '<input type="submit" value="Bestellung prüfen ..." class="toSummary" />';
	html += '</form>';
	return html;
}

/**
 * Navigation
 */
window.onhashchange = function() {
	surfSupport();
}
function surfSupport() {
	var fulldestination = document.location.hash;
	var destination = fulldestination;
	if (fulldestination == '') {
		$('.ticketbooking').hide();
		$('.ticketbooking').remove();
		$('.eventcatalogue').show();
		destination = '#overview';
	} else {
		var pathparts = fulldestination.split('/');
		if (pathparts.length == 1) {

		} else {
			// first is just "ticketshop"
			// second is only the id of the ticketshop
			// third = event id
			// 4 = page number
			if (pathparts[1] == undefined) {

			} else {
				if (pathparts[3] == undefined) {
					goBooking($('#' + pathparts[1]), pathparts[2]);
				} else {
					$('#' + pathparts[1] + ' .bpage').hide();
					$('#' + pathparts[1] + ' .bpage_' + pathparts[3]).show();
					$('#' + pathparts[1] + ' .stepmenu .step').removeClass('stepactive');
					$('#' + pathparts[1] + ' .stepmenu .step' + pathparts[3]).addClass('stepactive');
				}
				$('#' + pathparts[1])[0].scrollIntoView(true);
			}
		}
	}
}

function Generator() {
	return "uid" + Math.floor(Math.random() * 26) + Date.now();
}
;
function createUniqueId(element) {
	if ($(element).attr("id")) {
		// hat schon eine eindeutige id
	} else {
		var newid = Generator();
		while ($('#' + newid).length) {
			newid = Generator();
		}
		$(element).attr("id", newid);
	}
}

$(document).on('click',
	'.sessionselect',
	function() {
		/** optical indicator * */
		if ($(this).hasClass('presentation') && $(this).data('fullybooked') == '0') {
			if ($(this).data('selected') == '1') {
				$(this).data('selected', '')
				$(this).removeClass('selected');

				/**
				 * check those sessions which are linked to this one
				 */
				if ($(this).data('linked')) {
					var linkeditems = $(this).data('linked').split(",");
					for (i = 0; i < linkeditems.length; i++) {
						var linkedid = linkeditems[i].trim();
						var linkeditem = $(this).parent().parent().parent().find('[data-sessionid="' + linkedid + '"]');
						// $(linkeditem).parent().find('td').removeClass('selected');
						// $(linkeditem).parent().find('td').data('selected',
						// '');
						$(linkeditem).data('selected', 0);
						$(linkeditem).removeClass('selected');
					}
				}
			} else {
				$(this).parent().find('td').removeClass('selected');
				$(this).parent().find('td').data('selected', '');
				var others = $(this).parent().find('td');
				$(others).each(function(index) {
					if ($(this).data('linked')) {
						var linkeditems = $(this).data('linked').split(",");
						for (i = 0; i < linkeditems.length; i++) {
							var linkedid = linkeditems[i].trim();
							var linkeditem = $(this).parent().parent().parent().find('[data-sessionid="' + linkedid + '"]');
							$(linkeditem).data('selected', 0);
							$(linkeditem).removeClass('selected');
						}
					}
				});/*
				 * for (jj = 0; jj <
				 * $(others).length; jj ++) {
				 * $(others).removeClass('selected');
				 * $(others).data('selected',
				 * ''); if
				 * ($(others).data('linked')) {
				 * alert("link"); } }
				 */
				$(this).data('selected', 1);
				$(this).addClass('selected');

				/**
				 * check those sessions which are linked to this one
				 */
				if ($(this).data('linked')) {
					var linkeditems = $(this).data('linked').split(",");
					for (i = linkeditems.length - 1; i > -1; i--) {
						var linkedid = linkeditems[i].trim();
						var linkeditem = $(this).parent().parent().find('[data-sessionid="' + linkedid + '"]');
						// var linkeditem =
						// $('.sessionid_'+linkedid);//$(this).parent().parent().find('.sessionid_'+linkedid);
						$(linkeditem).parent().find('td').removeClass('selected');
						$(linkeditem).parent().find('td').data('selected', '');
						$(linkeditem).data('selected', 1);
						$(linkeditem).addClass('selected');
					}
				}
			}
			/** fill form field * */
			var selection = "";
			$(this).parent().parent().find('.selected').each(function(el) {
				selection += $(this).data('sessionid') + ",";
			});
			var selectionname = $(this).parent().parent().parent().data('selectionname');
			$(this).parent().parent().parent().parent().find('[name="' + selectionname + '"]').val(selection);
		}
	});
$(document).on('mouseenter', '.sessionselect.presentation', function() {
		// $(this).append('<div class="info">Info</div>');
		// $(this).find('.title').css('height', 'auto');
	});
$(document).on('mouseleave', '.sessionselect.presentation', function() {
		// $(this).find('.title').css('height', '');
		// $(this).find('.info').remove();
	});
$(document).on('click',
	'.sessionselect .sessionno',
	function() {
		var html = '';
		// var timecalc = "00:00 - 00:00";
		var from = $(this).parent().find('.datetime').html();
		var to = $(this).parent().find('.datetimeto').html();
		var timecalc = from.substring(8, 10) + ':' + from.substring(10, 12) + ' - ' + to.substring(8, 10) + ':' + to.substring(10, 12);
		$(this).parent().find('.datetime').html()
		html += '<img class="speakerpic" alt="' + $(this).parent().find('.speaker').html() + '" src="' + $(this).parent().find('.speakerpic').html() + '" />';
		html += '<h2>' + $(this).parent().find('.sessionnoh').html() + ' | ' + $(this).parent().find('.title').html() + '</h2>';
		html += '<p>' + $(this).parent().find('.speaker').html() + '</p>';
		html += '<p>' + timecalc + ' Uhr</p>';
		html += '<p>' + $(this).parent().find('.description').html() + '<p>';
		ticketpopup(html);
		return false;
	});
$(document).keyup(function(e) {
	if (e.keyCode == 27) { // escape key
		closeticketpopup();
	}
});
function ticketpopup(content) {
	$('.ticketpopupcontent').html('');
	$('body').append('<div class="ticketpopup"></div><div class="ticketpopupcontent">:-(</div>');
	$('.ticketpopupcontent').html('<div class="popupclose" alt="Popup schließen" title="Popup schließen">x</div>' + content);
	$('.ticketpopup').show();
	$('.ticketpopupcontent').show();
}
function closeticketpopup() {
	$('.ticketpopup').remove();
	$('.ticketpopupcontent').remove();
}
$(document).on('click', '.ticketpopup', function() {
		closeticketpopup();
	});
$(document).on('click', '.ticketpopupcontent', function() {
		closeticketpopup();
	});
$(document).on('click',
	'.step',
	function() {
		if (!$(this).hasClass('nodirectaccess')) {
			var eventid = $(this).parentsUntil('.ticketbooking').parent().data('eventid');
			var divid = $(this).parentsUntil('.ticketshop').parent().attr('id');
			goToBookingPage(divid, eventid, $(this).data('step'));
		}
	});
function goToBookingPage(divid, eventid, pageNo) {
	$('#' + divid).find('.ticketbooking').data('pageid', pageNo);
	var destination = "ticketshop/" + divid + "/" + eventid + "/" + pageNo;
	document.location.hash = destination;
}

$(document).ready(function() {
	$('.ticketshop.startbooking').each(function(key, val) {
		$(this).append('<div class="loading">Ticketshop wird geladen...</div>');
		createUniqueId(val);
		var eventid = $(this).data('eventid');
		goBooking(val, eventid);
	});
});
$(document).on('click',
	'.tcatchoice li',
	function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
		} else {
			if ($(this).parent().data('maximumchoices') == '1') {
				$(this).parent().find('li').removeClass('active');
			}
			$(this).addClass('active');
		}
		var selectedTxt = '';
		$(this).parent().find('li').each(function() {
			if ($(this).hasClass('active')) {
				selectedTxt += $(this).data('catid') + ",";
			}
		});
		var tcatchoicename = $(this).data('name');
		$(this).parentsUntil('.bookingform').find('[name="' + tcatchoicename + '"]').val(selectedTxt);
	});
$(document).on('click', '.foldhead', function() {
		$(this).parent().find('.foldcontent').toggleClass('hidden');
	});

/* Load fluxcess default CSS */
var cssId = fluxcessTicketsUrl + '/css/bookingstyle.css'; 
if (!document.getElementById(cssId)) {
	var head = document.getElementsByTagName('head')[0];
	var link = document.createElement('link');
	link.id = cssId;
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = fluxcessTicketsUrl + '/css/bookingstyle.css';
	link.media = 'all';
	head.appendChild(link);
}
$(document).on('submit',
	'.bookingform',
	function() {
		var currentPageId = $(this).parentsUntil('.ticketbooking').parent().data('pageid');
		var nextPage = currentPageId + 1;
		var divid = $(this).parentsUntil('.ticketshop').parent().attr('id');
		var eventid = $(this).parentsUntil('.ticketbooking').parent().data('eventid');
		var special = '';
		if ($('#' + divid + ' .bpage_' + currentPageId + ' .nextbutton').hasClass('buynow')) {
			special = 'buynow';
		}

		if ($(this).parentsUntil('.ticketbooking').parent().find('.bpage_' + nextPage).length == 0) {
			nextPage = currentPageId;
		}

		gotoNextPage(currentPageId, nextPage, divid, eventid,
			special);
		return false;
	});
$(document).on('submit',
	'.fluxcess_loginform',
	function() {
		$(this).find('.fluxcess_loadingMessage').show().html('Logging in ...');
		var currentPageId = $(this).parentsUntil('.ticketbooking').parent().data('pageid');
		var nextPage = currentPageId + 1;
		var divid = $(this).parentsUntil('.ticketshop').parent().attr('id');
		var eventid = $(this).parentsUntil('.ticketbooking').parent().data('eventid');
		var special = '';
		if ($('#' + divid + ' .bpage_' + currentPageId + ' .nextbutton').hasClass('buynow')) {
			special = 'buynow';
		}

		if ($(this).parentsUntil('.ticketbooking').parent().find('.bpage_' + nextPage).length == 0) {
			nextPage = currentPageId;
		}
		
		tryToLogin(eventid, $(this));

		return false;
	});

/**
 * Level 1 Login
 * This figures out the username and password typed into the form,
 * sends them off to fluxcess RSVP
 * and tries to authenticate there.
 * The authentication result (1 or 0) is evaluated,
 * and if it was successful (1), level 2 login is started.
 */
function tryToLogin(eventid, form) {
	var url = fluxcessRsvpUrl + "fevent/" + eventid + "/booking/logIn";
	var dtype = "POST";
	var bookinglinepart = null;
	var tjson = null;
	var data = null;
	data = {
		fevent_id: eventid,
		lastname: $(form).find('input[name=username]').val(),
		accessCode: $(form).find('input[name=password]').val()
	};
	$.ajax({
		type: dtype,
		url: url,
		data: data,
		headers: {
			'fluxcess': fluxcessSessionId 
		},
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				if (data.session.loggedin == 1) {
					fluxcessSessionId = data.data.sid;
					loginOnCurrentWebsite(data.data.sid, data.data.guest_id, form);
				} else {
					addErrorMessageToForm(form, 'Login failed, sorry.');	
				}
			}
		},
		complete: function() {
			$(form).find('.fluxcess_loadingMessage').hide();
		},
		dataType: 'json'
	});
}

/**
 * Level 2 Login
 * This calls a login url on the server of the custom website, 
 * handing over the PHP session id just received via Level 1 login. 
 * The server of the custom website is then expected to check 
 * validity of the session id on the fluxcess RSVP server - 
 * and either treat the user as "authenticated" or not.
 */
function loginOnCurrentWebsite(token, guestId, form) {
	var url = $('body').data('loginurl')+'&PHPSESSID=' + token;
	var dtype = "POST";
	var bookinglinepart = null;
	var tjson = null;
	var data = null;
	$(form).find('.fluxcess_waitMessage').show().html('Successful. Please wait...');
	data = {
		form: 'login',
		sid: token,
		PHPSESSID: token,
		guest_id: guestId
	};
	$.ajax({
		type: dtype,
		url: url,
		data: data,
		success: function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].msg + " ";
				}
				alert(errtxt);
			} else {
				if (data == "1") {
					location.reload();
				} else {
					addErrorMessageToForm(form, 'Apologies; The login failed.');
					$(form).find('.fluxcess_waitMessage').hide();
				}
			}
		},
		error: function() {
			$(form).find('.fluxcess_waitMessage').hide();
		},
		dataType: 'json'
	});
}

$(document).on('click', 'input.hideshow', function() {
		var hsstring = $(this).data('hideshow');
		var hsparts = hsstring.split(':');
		if (hsparts[0] && hsparts[1]) {
			var subject = '.fieldgroup_' + hsparts[0];
			if (hsparts[1] == 1) {
				$('.fieldgroup_' + hsparts[0]).show();
			} else {
				$(subject).hide();
			}
		}
	});
function has(object, key) {
	return object ? hasOwnProperty.call(object, key) : false;
}

/**
 * Adds an error message to the login form.
 * The login form needs to contain a field with class .fluxcess_errorMessage.
 * The error message will disappear automatically after a while.
 */
function addErrorMessageToForm(form, e) {
	$(form).find('.fluxcess_errorMessage').html(e);
	$(form).find('.fluxcess_errorMessage').show().fadeOut(1000);
}

/**
 * Sets a cookie
 */
function setCookie(name, value, hours) {
    var d = new Date;
    d.setTime(d.getTime() + 60*60*1000);
    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

/**
 * reads a cookie value
 */
function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : null;
}

/**
 * Adds a guest record to the RSVP form
 * ... technically, it only unhides it, as it does exist already.
 */
$(document).on('click',
	'.fluxcess_add_guest',
	function() {
		var element = findCurrentBookingElement($(this));
		var numberOfInActiveGuests = $(element).find('.fluxcess_perGuestForm[data-guestactive="0"]').length;
		$(element).find('[data-guestactive="0"]').first().removeClass("hidden").attr('data-guestactive', "1");
	}
);

/**
 * Removes a guest record to the RSVP form
 * ... technically, it only hides it.
 */
$(document).on('click',
	'.fluxcess_remove_guest',
	function() {
		$(this).parentsUntil('.fluxcess_perGuestForm').parent().addClass('hidden').attr('data-guestactive', '0');
	}
);
